require 'logger'

module Log
  extend self

  @log = Logger.new(STDOUT)

  @log.formatter = proc do |s, d, p, m|
    color = case s.to_s.downcase
              when 'debug'
                "\033[39m"
              when 'info'
                "\033[32m"
              when 'warn', 'unknown'
                "\033[35m"
              when 'error', 'fatal'
                "\033[31m"
              else
                "\033[39m"
            end

    "#{color}[#{d.strftime('%Y-%m-%d %H:%M:%S')}] (#{p}) #{m}\033[0m\n"
  end

  def method_missing(method, *args, &block)
    @log.send(method, *args, &block)
  end
end