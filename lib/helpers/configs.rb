module Configs
  extend self

  def [](conf)
    load unless @configs
    @configs[conf.to_sym]
  end

  def load
    @configs = {}

    Dir['config/**/*.yml'].each do |file|
      config_key = file_to_sym(file)
      @configs[config_key] = YAML::load_file(file)
      @configs[config_key].each { |k, v| @configs[config_key][k] = ENV[k.to_s.upcase] || v }
    end
  end

  def file_to_sym(file)
    file.split('/').last.split('.').first.to_sym
  end

  load
end