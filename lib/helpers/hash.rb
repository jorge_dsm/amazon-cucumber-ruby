class Hash
  def uniform
    new_h = {}
    each do |k, v|
      new_k = k.to_s.gsub(/[\s-]+/, '_').downcase.to_sym

      new_v = case v.to_s
                when 'true'
                  true
                when 'false'
                  false
                when /^\/.*\/$/
                  Regexp.new(v[1..-2])
                when /^\d+$/
                  v.to_i
                else
                  v
              end

      new_h[new_k] = new_v
    end
    new_h
  end
end