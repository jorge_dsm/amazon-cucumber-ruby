module Amazon
  class Product
    include PageObject

    # product fields
    span(:title, id: 'productTitle')
    span(:author, class: %w(author notFaded))
    div(:description, class: %w(DigitalVideoUI_Expander__expander DigitalVideoWebNodeDetail_synopsis__synopsis))

    # watch trailer button
    link(:watch_trailer, class: %w(DigitalVideoUI_Button__forceDouble DigitalVideoUI_Button__withIcons DigitalVideoUI_Button__default DigitalVideoUI_Button__button js-deeplinkable))

    # customer reviews
    link(:customer_reviews, id: 'acrCustomerReviewLink')
    div(:review_list, id: 'cm-cr-dp-review-list')
    div(:review_list_open, id: 'cm_cr-review_list')
    button(:write_a_customer_review, text: 'Write a customer review')
    table(:reviews_table, id: 'histogramTable')

    def check_fields(params)
      params.each do |field, expected|
        actual = send(field)
        Log.debug(self.class) {"Field #{field} has text: #{actual}"}

        if actual.include?(expected)
          Log.info(self.class) {"Field #{field} has expected text!"}
        else
          raise("Field #{field} does not have expected text!")
        end
      end
    end

    def customer_reviews_are?(amount)
      num_reviews = customer_reviews_element.text[/^\d+/]
      Log.debug(self.class) {"The product has #{num_reviews} reviews."}

      if eval("#{num_reviews}#{amount}")
        Log.info(self.class) {"The product reviews are #{amount}!"}
      else
        raise("The product reviews are not #{amount}!")
      end
    end

    def search_customer_review(params)
      list = review_list? ? review_list_element : review_list_open_element

      list.divs(class: %w(a-section review)).each do |review_div|
        info = get_review_info(review_div)
        review_matches = true

        params.each do |field, expected|
          break unless (review_matches = info[field] && info[field].match(expected))
        end

        if review_matches
          Log.info(self.class) {'Found a review that matches your criteria!'}
          print_review(info)
          return
        end
      end

      raise('Did not find a review that matches your criteria!')
    end

    def go_to_reviews_with_rating(rating)
      reviews_table_element.link(title: "#{rating} star").click
    end

    private
    def get_review_info(div)
      whole_text = div.text

      review_text_element = div.div(class: %w(a-expander-content a-expander-partial-collapse-content))
      review_text_element = div.span(class: %w(a-size-base review-text)) unless review_text_element.present?

      {
          user: div.span(class: 'a-profile-name').text,
          stars: div.span(class: 'a-icon-alt').text[/\d/],
          title: div.link(class: %w(a-size-base a-link-normal review-title a-color-base a-text-bold)).text,
          date: div.span(class: %w(a-size-base a-color-secondary review-date)).text,
          format: whole_text.match(/format:\s*([\w ]+)/i) ? $1 : nil,
          verified_purchase: whole_text.include?('Verified Purchase'),
          review_text: review_text_element.text
      }
    end

    def print_review(info)
      print_info = ''
      info.each do |k, v|
        print_info += "\n#{k.to_s.split('_').map(&:capitalize).join(' ')}: #{v}"
      end

      Log.debug(self.class) {"Review Info:#{print_info}"}
    end

  end
end