module Amazon
  class SignIn
    include PageObject

    # divs
    div(:error_info, id: 'auth-error-message-box')

    # text fields
    text_field(:email, id: 'ap_email')
    text_field(:password, id: 'ap_password')

    # buttons
    button(:sign_in, id: 'signInSubmit')
    link(:create_your_amazon_account, id: 'createAccountSubmit')


    # methods

    def no_error!
      error_text = ''

      if !error_info? || (error_text = error_info).empty?
        Log.info(self.class) {'No error was found!'}
      else
        raise("An error was found: #{error_text}")
      end
    end

    def error_contains?(text)
      error_text = error_info
      Log.debug(self.class) {"Authentication error: #{error_text}"}

      if error_text.downcase.include?(text.downcase)
        Log.info(self.class) {'Error page has expected text!'}
      else
        raise("Error Page does not have expected text '#{text}'")
      end
    end

  end
end