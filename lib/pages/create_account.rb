module Amazon
  class CreateAccount
    include PageObject

    # divs
    div(:error_info, class: %w(a-box a-alert a-alert-warning a-spacing-base))

    # text fields
    text_field(:your_name, id: 'ap_customer_name')
    text_field(:email, id: 'ap_email')
    text_field(:password, id: 'ap_password')
    text_field(:re_enter_password, id: 'ap_password_check')

    # buttons
    button(:create_your_amazon_account, id: 'continue')


    # methods

    def error_contains?(text)
      error_text = error_info
      Log.debug(self.class) {"Authentication error: #{error_text}"}

      if error_text.downcase.include?(text.downcase)
        Log.info(self.class) {'Error page has expected text!'}
      else
        raise("Error Page does not have expected text '#{text}'")
      end
    end

  end
end