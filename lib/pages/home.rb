module Amazon
  class Home
    include PageObject

    # text_fields
    text_field(:search_box, id: 'twotabsearchtextbox')

    # buttons
    button(:search, value: 'Go')
    link(:sign_in_your_account, id: 'nav-link-yourAccount')

    # search result list
    unordered_list(:results, id: 's-results-list-atf')

    def search_results_are?(amount)
      num_results = results_element.lis.size
      Log.debug(self.class) {"The search returned #{num_results} results."}

      if eval("#{num_results}#{amount}")
        Log.info(self.class) {"Search results are #{amount}!"}
      else
        raise("Search results are not #{amount}!")
      end
    end

    def click_result(index=0)
      result = results_element.lis[index]
      product_link = result.link(class: %w(a-link-normal s-access-detail-page  s-color-twister-title-link a-text-normal))

      Log.info(self.class) {"Clicking on result: #{product_link.title}"}

      product_link.click
    end

    def click_result_with_title(title)
      results_element.lis.each do |result|
        product_link = result.link(class: %w(a-link-normal s-access-detail-page  s-color-twister-title-link a-text-normal))

        if product_link.title.include?(title)
          Log.info(self.class) {"Clicking on result: #{product_link.title}"}
          product_link.click
          return
        end
      end

      raise("No search result was found with title '#{title}'")
    end

  end
end