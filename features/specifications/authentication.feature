Feature: Account Creation and Sign-In functionality

  Background:
    Given I go to the Amazon website


  @1 @no-sign-in
  Scenario: Create Account with email already in use
    When I create an account with parameters
      | Your name  | Email      | Password | Re-enter password |
      | Jane Dough | jdsm@ua.pt | 123456   | 123456            |
    Then I get an error creating account with text "E-mail address already in use"


  @2 @sign-in
  Scenario: Sign-In with invalid password
    When I sign-in with parameters
      | email      | password |
      | jdsm@ua.pt | 123456   |
    Then I get an error signing in with text "Your password is incorrect"


  @3 @sign-in
  Scenario: Sign-In with valid username/password
    When I sign-in with parameters
      | email      | password |
      | jdsm@ua.pt | 456890   |
    Then I get no error signing in
