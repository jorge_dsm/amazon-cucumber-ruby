Feature: Product Search and Validation - using a signed in user

  Background:
    Given I go to the Amazon website
    And I sign-in with parameters
      | email      | password |
      | jdsm@ua.pt | 456890   |
    And I get no error signing in


  @4 @sign-in
  Scenario: Book search and validation

    # 4.1
    When I search for "Chasing Excellence"
    Then The search should have >0 result

    # 4.2
    Given I click the search result number 1
    Then I check the opened product with parameters
      | Title              | Author   |
      | Chasing Excellence | Bergeron |

    # 4.3
    Given On the current product I go to Customer Reviews
    Then The product should have >0 reviews
    And I check for reviews with parameters
      | User                    | Review Text |
      | Cerith Leighton Watkins | /.+/        |

    # 4.4
    # Sorry, you do not yet meet the minimum eligibility requirements to write a review on Amazon.
    # To contribute to Community Features (for example, Customer Reviews, Customer Answers),
    # you must have spent at least £40 on Amazon.co.uk using a valid payment card in the past 12 months.

    # 4.5
    Given I go to the 1 star Customer Reviews

    # 4.6
    And I check for reviews with parameters
      | Date              | Review Text |
      | 17 September 2017 | /.+/        |


  @5 @sign-in
  Scenario: Movie search and validation

    # 5.1
    When I search for "avengers"
    Then The search should have >0 result

    # 5.2
    Given I click the search result with title "Avengers Assemble"
    Then I check the opened product with parameters
      | Description |
      | S.H.I.E.L.D |

    # 5.3
    When On the current product I watch the trailer
    And I wait until the trailer ends