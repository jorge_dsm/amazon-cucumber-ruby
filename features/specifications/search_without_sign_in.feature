Feature: Product Search and Validation - without signing in

  Background:
    Given I go to the Amazon website

  @4 @no-sign-in
  Scenario: Book search and validation

    # 4.1
    When I search for "Chasing Excellence"
    Then The search should have >0 result

    # 4.2
    Given I click the search result number 1
    Then I check the opened product with parameters
      | Title              | Author   |
      | Chasing Excellence | Bergeron |

    # 4.3
    Given On the current product I go to Customer Reviews
    Then The product should have >0 reviews
    And I check for reviews with parameters
      | User                    | Review Text |
      | Cerith Leighton Watkins | /.+/        |

    # 4.5
    Given I go to the 1 star Customer Reviews

    # 4.6
    And I check for reviews with parameters
      | Date              | Review Text |
      | 17 September 2017 | /.+/        |


  @5 @no-sign-in
  Scenario: Movie search and validation

    # 5.1
    When I search for "avengers"
    Then The search should have >0 result

    # 5.2
    Given I click the search result with title "Avengers Assemble"
    Then I check the opened product with parameters
      | Description |
      | S.H.I.E.L.D |

    # 5.3
    When On the current product I watch the trailer
    And I wait until the trailer ends