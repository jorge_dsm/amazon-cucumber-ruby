require 'page-object'
require 'watir'

# helper modules
Dir["#{__dir__}/../../lib/helpers/**/*.rb"].each do |file|
  require file
end

# pages
Dir["#{__dir__}/../../lib/pages/**/*.rb"].each do |file|
  require file
end

# Page-Object world module
World(PageObject::PageFactory)