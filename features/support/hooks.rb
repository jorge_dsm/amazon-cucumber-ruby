Before do |scenario|
  Log.info('Started') {"Scenario: #{scenario.name}"}
  @browser = Watir::Browser.new(Configs[:selenium][:browser], headless: Configs[:selenium][:headless])
  @browser.window.maximize if Configs[:selenium][:maximize]
end

After do |scenario|
  sleep Configs[:selenium][:sleep_after] if Configs[:selenium][:sleep_after] > 0
  @browser.close

  if scenario.passed?
    Log.info('Finished') {"Scenario Passed: #{scenario.name}\n"}
  else
    Log.error('Finished') {"Scenario Failed: #{scenario.name}\n"}
  end
end