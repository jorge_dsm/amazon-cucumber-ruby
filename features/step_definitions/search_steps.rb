When(/^I search for "([^"]*)"$/) do |search_text|
  on(Amazon::Home) do |page|
    page.search_box = search_text
    page.search
  end
end

Then(/^The search should have ([<>]?\d+) results?$/) do |amount|
  on(Amazon::Home) do |page|
    page.search_results_are?(amount)
  end
end

Given(/^I click the search result number (\d+)$/) do |index|
  on(Amazon::Home) {|page| page.click_result(index - 1)}
end

Given(/^I click the search result with title "([^"]+)"$/) do |title|
  on(Amazon::Home) {|page| page.click_result_with_title(title)}
end