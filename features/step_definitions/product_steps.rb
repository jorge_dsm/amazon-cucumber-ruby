Then(/^I check the opened product with parameters$/) do |table|
  on(Amazon::Product) do |page|
    page.check_fields(table.hashes.first.uniform)
  end
end

Given(/^On the current product I go to Customer Reviews/) do
  on(Amazon::Product) {|page| page.customer_reviews}
end

Then(/^The product should have ([<>]?\d+) reviews$/) do |amount|
  on(Amazon::Product) {|page| page.customer_reviews_are?(amount)}
end

Then(/^I check for reviews with parameters$/) do |table|
  on(Amazon::Product) do |page|
    table.hashes.each do |params|
      page.search_customer_review(params.uniform)
    end
  end
end

Given(/^I go to the (\d) star Customer Reviews$/) do |rating|
  on(Amazon::Product) {|page| page.go_to_reviews_with_rating(rating)}
end

When(/^On the current product I watch the trailer$/) do
  on(Amazon::Product) do |page|
    page.watch_trailer
    Watir::Wait.until(5) {!page.watch_trailer?}
  end
end

When(/^I wait until the trailer ends$/) do
  on(Amazon::Product) {|page| Watir::Wait.until(300) {page.watch_trailer?}}
end