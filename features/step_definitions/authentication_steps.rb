Given(/^I create an account with parameters$/) do |table|
  on(Amazon::Home) {|page| page.sign_in_your_account}
  on(Amazon::SignIn) {|page| page.create_your_amazon_account}

  on(Amazon::CreateAccount) do |page|
    page.populate_page_with(table.hashes.first.uniform)
    page.create_your_amazon_account
  end
end

Then(/^I get an error creating account with text "([^"]+)"$/) do |expected|
  on(Amazon::CreateAccount) {|page| page.error_contains?(expected)}
end

Given(/^I sign-in with parameters/) do |table|
  on(Amazon::Home) {|page| page.sign_in_your_account}

  on(Amazon::SignIn) do |page|
    page.populate_page_with(table.hashes.first.uniform)
    page.sign_in
  end
end

Then(/^I get no error signing in$/) do
  on(Amazon::SignIn) {|page| page.no_error!}
end

Then(/^I get an error signing in with text "([^"]+)"$/) do |expected|
  on(Amazon::SignIn) {|page| page.error_contains?(expected)}
end